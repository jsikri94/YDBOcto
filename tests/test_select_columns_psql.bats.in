#################################################################
#								#
# Copyright (c) 2019-2022 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

# Note: This test is similar to `test_select_columns.bats.in` except that the subtest here use
# `run_query_in_octo_and_postgres_and_crosscheck_multiple_queries` and hence need to be somewhere outside
# of the `test_select_columns.bats.in` test (see end of `test_select_columns.bats.in` for comment explaining why).

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
}

@test "TSCP01 : OCTO466 : Incorrect results when '|' is part of the column value in the SELECT column list" {
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP01.sql
}

@test "TSCP02 : OCTO544 : Assertion failure and Errors when IN is used in SELECT column list" {
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP02.sql
  load_fixture TSCP02_errors.sql subtest novv
  verify_output TSCP02_errors output.txt
}

@test "TSCP03 : OCTO552 : Allow booleans anywhere that takes a value_expression" {
  load_fixture nullnames.sql
  load_fixture nullnames.zwr
  load_fixture boolean.sql
  load_fixture boolean.zwr
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP03_names.sql
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries nullnames TSCP03_nullnames.sql
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries boolean TSCP03_boolean.sql
  load_fixture TSCP03_errors.sql subtest novv
  verify_output TSCP03_errors output.txt
}

@test "TSCP04 : OCTO561 : Support ROUND() function" {
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP04.sql
  load_fixture TSCP04_errors.sql subtest novv
  verify_output TSCP04_errors output.txt
}

@test "TSCP05 : OCTO560 : Support TRUNC() function" {
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP05.sql
  load_fixture TSCP05_errors.sql subtest novv
  verify_output TSCP05_errors output.txt
}

@test "TSCP06 : OCTO469 : SELECT COLUMN list values ~ 1Mb long AND 1024 columns in Octo/Rocto; Also test OCTO473 and OCTO474" {
  $ydb_dist/mupip set -key_size=1019 -record_size=1048576 -reg DEFAULT	# huge records are going to be created in next line
  inputfile="TSC20.sql"
  cp @PROJECT_SOURCE_DIR@/tests/fixtures/$inputfile .
  yottadb -run TSC20	# populate ^longvalues and ^lotsofcols globals, creates ddl.sql AND creates TSC20.ref
  grep -v '^#' $inputfile > output.txt	# Filter out copyright from output
  # Load CREATE TABLE commands for LONGVALUES and LOTSOFCOLS
  octo -f ddl.sql > octo_output.txt.tmp 2>&1
  # Test Octo
  octo -f $inputfile > octo_output.txt.tmp 2>&1
  # Filter global octo.conf (any not in the local directory)
  sed -i '/^.*\[ INFO\].* \/.*\/octo.conf/d' octo_output.txt.tmp
  # Filter out header ("ID" or "VALUE" or "COL1") and summary ("rows") lines from octo output before creating reference file.
  grep -vwE "ID|VALUE|COL1|rows" octo_output.txt.tmp > octo_output.txt
  # Generate the final reference file (it is two copies of what has been generated in TSC20.half_ref)
  cat TSC20.half_ref TSC20.half_ref > TSC20.ref
  cmp TSC20.ref octo_output.txt
  # Test Rocto with same set of queries
  create_user ydb tester
  test_port=$(start_rocto 1339)
  run_psql_auth ydb tester $test_port < $inputfile > rocto_orig_output.txt
  stop_rocto
  delete_users ydb
  grep -vwE "id|value|rows" rocto_orig_output.txt > rocto_output.txt
  # Filter global octo.conf (any not in the local directory)
  sed -i '/^.*\[ INFO\].* \/.*\/octo.conf/d' rocto_output.txt
  cmp TSC20.ref rocto_output.txt
}

@test "TSCP07 : OCTO576 : Numeric literals in SELECT column list should be displayed as is" {
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP07.sql
}

@test "TSCP08 : OCTO385/OCTO386 : Verify different asterisk and TABLENAME.ASTERISK usage in select column list and ORDER BY" {
  # TSCP08.sql has 100+ queries so run only a small random fraction (25%) of them to cut down test runtime.
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP08.sql 0.25
  load_fixture customers.sql
  load_fixture customers.zwr
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries customers TSCP08_customers.sql
  load_fixture TSCP08_errors.sql subtest novv
  verify_output TSCP08_errors output.txt
}

@test "TSCP09 : OCTO370/OCTO623/OCTO629 : Test of string concatenation in select column list" {
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP09.sql "trim_trailing_zeroes"
}

@test "TSCP10 : OCTO628 : RPARENMISSING error when trying to use || operator with hundreds of operands" {
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP10.sql
}

@test "TSCP11 : OCTO386 : Verify different TABLENAME.ASTERISK usage with GROUP BY and aggregate functions" {
  # TSCP11.sql has 100+ queries so run only a small random fraction (25%) of them to cut down test runtime.
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP11.sql 0.25
  load_fixture customers.sql
  load_fixture customers.zwr
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries customers TSCP11_customers.sql
  load_fixture TSCP11_errors.sql subtest novv
  verify_output TSCP11_errors output.txt
}

@test "TSCP12 : OCTO386 : Verify count(DISTINCT n1.id) and count(DISTINCT n1.*) creates two different physical plans" {
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP12_column.sql
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP12_table_asterisk.sql
  [[ $(ls -l _ydboctoP*.m | wc -l) -eq 2 ]]
}

@test "TSCP13 : OCTO386 : Verify count(DISTINCT n1.*) and count(n1.*) behavior with single column table created through DDL" {
  load_fixture names1col.sql
  load_fixture names1col.zwr
  run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names1col TSCP13.sql
  load_fixture TSCP13_errors.sql subtest novv
  verify_output TSCP13_errors output.txt
}

@test "TSCP14 : OCTO386 : Verify AVG/SUM/MIN/MAX issue error with table.* even if table has only one column" {
	echo "# Verify AVG/SUM/MIN/MAX work fine if done on table.col syntax (1 column)" > final_output.txt
	load_fixture names1col.sql
	load_fixture names1col.zwr
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP14_names.sql "trim_trailing_zeroes"
	echo "Cross check on TSCP14_names.sql PASSED" >> final_output.txt
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names1col TSCP14_names1col.sql "trim_trailing_zeroes"
	echo "Cross check on TSCP14_names1col.sql PASSED" >> final_output.txt
	echo "# Verify AVG/SUM/MIN/MAX issue error with table.* even if table has only one column" >> final_output.txt
	load_fixture TSCP14_errors.sql subtest novv
	cat output.txt >> final_output.txt
	mv final_output.txt output.txt
	verify_output TSCP14 output.txt
}

# Note: TSCP15 subtest was removed as part of YDBOcto#759 code fixes (queries from there were moved to TTA003.sql)

@test "TSCP16 : OCTO386 : Pipeline failure on centos related to outer join, order by and table.* usage" {
	load_fixture nullnames.sql
	load_fixture nullnames.zwr
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries nullnames TSCP16.sql
}

@test "TSCP17 : OCTO779 : Incorrect ERR_UNKNOWN_COLUMN_NAME error when GROUP BY is used on a VALUES table in the JOIN list" {
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP17.sql
}

@test "TSCP18 : OCTO288 : Test functions now(), lpad(), localtimestamp(), localtime(), current_timestamp(), current_time()" {
	run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP18.sql
}

@test "TSCP19: OCTO831 : Generate an error when the IN and NOT IN list has different types" {
	load_fixture TSCP19_errors.sql subtest novv
	verify_output TSCP19_errors output.txt
}

@test "TSCP20 : OCTO500 : Support SELECT without FROM when WHERE is present" {
       run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP20.sql
}

@test "TSCP21 : OCTO854 : Support subquery in in-list " {
       run_query_in_octo_and_postgres_and_crosscheck_multiple_queries names TSCP21.sql
       load_fixture TSCP21_errors.sql subtest novv
       verify_output TSCP21_errors output.txt
}
