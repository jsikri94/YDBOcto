
-- TDFT03 : OCTO54 : Test various errors in DELETE FROM

select '-- Test of ERR_UNKNOWN_COLUMN_NAME';
delete from names where abcd = 2;

select '-- Test of ERR_MISSING_FROM_ENTRY';
delete from names n1 where names.id = 2;

select '-- Test of ERR_TABLE_READONLY error against various catalog tables';
delete from octoOneRowTable;
delete from pg_catalog.pg_namespace where NULL != NULL;
delete from pg_catalog.pg_type;
delete from pg_catalog.pg_class;
delete from pg_catalog.pg_description;
delete from information_schema.tables where NULL = NULL;
delete from pg_catalog.pg_proc;
delete from pg_catalog.pg_attribute;
delete from pg_catalog.pg_attrdef;
delete from pg_catalog.pg_settings;
delete from pg_catalog.pg_database;
delete from pg_catalog.pg_roles;
delete from pg_catalog.pg_user;

select '-- Test of ERR_TABLE_READONLY error in a table created with READONLY';
create table test1 (id INTEGER PRIMARY KEY) READONLY;
delete from test1;
drop table test1;

select '-- Test same table created with READWRITE works fine with DELETE FROM';
create table test1 (id INTEGER PRIMARY KEY) READWRITE;
delete from test1;
drop table test1;

select '-- Test of ERR_TABLE_READONLY error in a table created with READONLY implicitly assumed';
create table test1 (id INTEGER PRIMARY KEY, firstname VARCHAR DELIM "", lastname VARCHAR DELIM "");
delete from test1;
drop table test1;

select '-- Test of ERR_UNKNOWN_TABLE error';
delete from abcd;

select '-- Test of ERR_TYPE_NOT_COMPATIBLE error';
delete from names where firstname;

select '-- Test of ERR_TYPE_MISMATCH error';
delete from names where lastname = id; -- a modified version of this query (with a type cast operator) is tested in TDFT01.sql

???
-- Test of ERR_UNKNOWN_COLUMN_NAME
(1 row)
[ERROR]: ERR_UNKNOWN_COLUMN_NAME: Unknown column: ABCD
LINE 1: delete from names where abcd = 2;
                                ^^^^
???
-- Test of ERR_MISSING_FROM_ENTRY
(1 row)
[ERROR]: ERR_MISSING_FROM_ENTRY: Missing FROM-clause entry for table : NAMES
LINE 1: delete from names n1 where names.id = 2;
                                   ^^^^^^^^
???
-- Test of ERR_TABLE_READONLY error against various catalog tables
(1 row)
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'OCTOONEROWTABLE'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_NAMESPACE'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_TYPE'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_CLASS'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_DESCRIPTION'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'INFORMATION_SCHEMA.TABLES'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_PROC'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_ATTRIBUTE'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_ATTRDEF'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_SETTINGS'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_DATABASE'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_ROLES'. Only allowed on READWRITE tables.
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'PG_CATALOG.PG_USER'. Only allowed on READWRITE tables.
???
-- Test of ERR_TABLE_READONLY error in a table created with READONLY
(1 row)
CREATE TABLE
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'TEST1'. Only allowed on READWRITE tables.
DROP TABLE
???
-- Test same table created with READWRITE works fine with DELETE FROM
(1 row)
CREATE TABLE
DELETE 0
DROP TABLE
???
-- Test of ERR_TABLE_READONLY error in a table created with READONLY implicitly assumed
(1 row)
CREATE TABLE
[ERROR]: ERR_TABLE_READONLY: DELETE not allowed on READONLY table 'TEST1'. Only allowed on READWRITE tables.
DROP TABLE
???
-- Test of ERR_UNKNOWN_TABLE error
(1 row)
[ERROR]: ERR_UNKNOWN_TABLE: Unknown table: ABCD
LINE 1: delete from abcd;
                    ^^^^
???
-- Test of ERR_TYPE_NOT_COMPATIBLE error
(1 row)
[ERROR]: ERR_TYPE_NOT_COMPATIBLE: Type VARCHAR not compatible for boolean operations
LINE 1: delete from names where firstname;
                                ^^^^^^^^^
???
-- Test of ERR_TYPE_MISMATCH error
(1 row)
[ERROR]: ERR_TYPE_MISMATCH: Type mismatch: left VARCHAR, right INTEGER
LINE 1: delete from names where lastname = id; -- a modified version of ...
                                ^^^^^^^^
LINE 1: ...te from names where lastname = id; -- a modified version of this...
                                          ^^
