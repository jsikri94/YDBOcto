
-- TBT03: Test of BOOLEAN type across various queries with errors OR different output between Octo & Postgres
CREATE FUNCTION DOLLARZWRITE(INTEGER) RETURNS VARCHAR AS $ZWRITE;

-- Test that unary operator on a boolean value issues error
select +id::boolean from names;
select -id::boolean from names;

-- Test that numeric type cast to boolean does not issue error (northwind database)
-- Note: Postgres issues an error for this but Octo does not since in M numeric and integer are the same.
select Price::boolean from Products;

-- Test that string type cast to boolean issues error
select firstname::boolean from names;
select DOLLARZWRITE(id)::boolean from names;

-- Test GROUP BY and AGGREGATE FUNCTIONs (COUNT, MIN, MAX, SUM, AVG etc.) using boolean column
-- MIN/MAX/SUM/AVG does not work with boolean types. Only COUNT works.
select min(mybool) from (select id=2 as mybool from names) n1;
select max(mybool) from (select id=2 as mybool from names) n1;
select sum(mybool) from (select id=2 as mybool from names) n1;
select avg(mybool) from (select id=2 as mybool from names) n1;

-- Test set operations on boolean and non-boolean type columns errors out
select id=2 from names union select id from names;
select id=2 from names union select firstname from names;
select Price=2.5 from Products union select Price from Products;

CREATE FUNCTION
[ERROR]: ERR_INVALID_INPUT_SYNTAX: Invalid input syntax : Expecting type NUMERIC or INTEGER : Actual type BOOLEAN
LINE 1: select +id::boolean from names;
                ^^
[ERROR]: ERR_INVALID_INPUT_SYNTAX: Invalid input syntax : Expecting type NUMERIC or INTEGER : Actual type BOOLEAN
LINE 1: select -id::boolean from names;
                ^^
PRICE
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
(77 rows)
[ERROR]: ERR_INVALID_INPUT_SYNTAX_BOOL: Invalid input syntax for type boolean: 'Zero' is not a valid boolean value
[ERROR]: ERR_INVALID_INPUT_SYNTAX_BOOL: Invalid input syntax for type boolean: '2' is not a valid boolean value
[ERROR]: ERR_MISTYPED_FUNCTION: Function MIN cannot be invoked with a parameter of type BOOLEAN
LINE 1: select min(mybool) from (select id=2 as mybool from names) n1;
               ^^^
[ERROR]: ERR_MISTYPED_FUNCTION: Function MAX cannot be invoked with a parameter of type BOOLEAN
LINE 1: select max(mybool) from (select id=2 as mybool from names) n1;
               ^^^
[ERROR]: ERR_MISTYPED_FUNCTION: Function SUM cannot be invoked with a parameter of type BOOLEAN
LINE 1: select sum(mybool) from (select id=2 as mybool from names) n1;
               ^^^
[ERROR]: ERR_MISTYPED_FUNCTION: Function AVG cannot be invoked with a parameter of type BOOLEAN
LINE 1: select avg(mybool) from (select id=2 as mybool from names) n1;
               ^^^
[ERROR]: ERR_SETOPER_TYPE_MISMATCH: UNION types BOOLEAN and INTEGER cannot be matched
LINE 1: select id=2 from names union select id from names;
               ^^^^
LINE 1: select id=2 from names union select id from names;
                                            ^^
[ERROR]: ERR_SETOPER_TYPE_MISMATCH: UNION types BOOLEAN and VARCHAR cannot be matched
LINE 1: select id=2 from names union select firstname from names;
               ^^^^
LINE 1: select id=2 from names union select firstname from names;
                                            ^^^^^^^^^
[ERROR]: ERR_SETOPER_TYPE_MISMATCH: UNION types BOOLEAN and NUMERIC cannot be matched
LINE 1: select Price=2.5 from Products union select Price from Products;
               ^^^^^^^^^
LINE 1: select Price=2.5 from Products union select Price from Products;
                                                    ^^^^^
