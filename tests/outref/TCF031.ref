
-- TCF031 : OCTO816 : Octo issues error for ambiguous function calls

CREATE FUNCTION samevalue(integer) RETURNS integer AS $$a1^TCF031;
SELECT samevalue(NULL); -- Call $$a1^TCF031: return 1
SELECT samevalue(1); -- Call $$a1^TCF031: return 1
SELECT samevalue(1.0); -- Error: Unknown function
SELECT samevalue('1'); -- Error: Unknown function
SELECT samevalue(true); -- Error: Unknown function
CREATE FUNCTION samevalue(numeric) RETURNS integer AS $$a2^TCF031;
SELECT samevalue(NULL); -- Error: Function not unique
SELECT samevalue(1); -- Call $$a1^TCF031: return 1
SELECT samevalue(1.0); -- Call $$a2^TCF031: return 2
SELECT samevalue('1'); -- Error: Unknown function
SELECT samevalue(true); -- Error: Unknown function
CREATE FUNCTION samevalue(varchar) RETURNS integer AS $$a3^TCF031;
SELECT samevalue(NULL); -- Error: Function not unique
SELECT samevalue(1); -- Call $$a1^TCF031: return 1
SELECT samevalue(1.0); -- Call $$a2^TCF031: return 2
SELECT samevalue('1'); -- Call $$a2^TCF031: return 3
SELECT samevalue(true); -- Error: Unknown function
CREATE FUNCTION samevalue(boolean) RETURNS integer AS $$a4^TCF031;
SELECT samevalue(NULL); -- Error: Function not unique
SELECT samevalue(1); -- Call $$a1^TCF031: return 1
SELECT samevalue(1.0); -- Call $$a2^TCF031: return 2
SELECT samevalue('1'); -- Call $$a2^TCF031: return 3
SELECT samevalue(true); -- Call $$a4^TCF031: return 4

CREATE FUNCTION secondvalue(integer, integer) RETURNS integer AS $$b1^TCF031;
SELECT secondvalue(NULL, NULL); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, NULL); -- Call $$b1^TCF031: return 1
SELECT secondvalue(NULL, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1.0, 1); -- Error: Unknown function
CREATE FUNCTION secondvalue(integer, numeric) RETURNS integer AS $$b2^TCF031;
SELECT secondvalue(NULL, NULL); -- Error: Function not unique
SELECT secondvalue(1, NULL); --  Error: Function not unique
SELECT secondvalue(1.0, 1); -- Error: Unknown function
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1.0); -- Call $$b2^TCF031: return 2
CREATE FUNCTION secondvalue(integer, varchar) RETURNS integer AS $$b3^TCF031;
SELECT secondvalue(NULL, NULL); -- Error: Function not unique
SELECT secondvalue(1, NULL); --  Error: Function not unique
SELECT secondvalue('1', 1); -- Error: Unknown function
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1.0); -- Call $$b2^TCF031: return 2
SELECT secondvalue(NULL, '1'); -- Call $$b3^TCF031: return 3
SELECT secondvalue(1, '1'); -- Call $$b3^TCF031: return 3
CREATE FUNCTION secondvalue(integer, boolean) RETURNS integer AS $$b4^TCF031;
SELECT secondvalue(NULL, NULL); -- Error: Function not unique
SELECT secondvalue(1, NULL); --  Error: Function not unique
SELECT secondvalue(true, 1); -- Error: Unknown function
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1.0); -- Call $$b2^TCF031: return 2
SELECT secondvalue(1, '1'); -- Call $$b3^TCF031: return 3
SELECT secondvalue(NULL, true); -- Call $$b4^TCF031: return 4
SELECT secondvalue(1, true); -- Call $$b4^TCF031: return 4
CREATE FUNCTION secondvalue(varchar, integer) RETURNS integer AS $$b5^TCF031;
SELECT secondvalue(NULL, NULL); -- Error: Function not unique
SELECT secondvalue(NULL, 1); --  Error: Function not unique
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1.0); -- Call $$b2^TCF031: return 2
SELECT secondvalue(1, '1'); -- Call $$b3^TCF031: return 3
SELECT secondvalue(NULL, true); -- Call $$b4^TCF031: return 4
SELECT secondvalue(1, true); -- Call $$b4^TCF031: return 4
SELECT secondvalue('1', NULL); -- Call $$b5^TCF031: return 5
SELECT secondvalue('1', 1); -- Call $$b5^TCF031: return 5
CREATE FUNCTION secondvalue(varchar, varchar) RETURNS integer AS $$b6^TCF031;
SELECT secondvalue(NULL, NULL); -- Error: Function not unique
SELECT secondvalue(NULL, '1'); --  Error: Function not unique
SELECT secondvalue('1', NULL); --  Error: Function not unique
SELECT secondvalue(1, 1); -- Call $$b1^TCF031: return 1
SELECT secondvalue(1, 1.0); -- Call $$b2^TCF031: return 2
SELECT secondvalue(1, '1'); -- Call $$b3^TCF031: return 3
SELECT secondvalue(NULL, true); -- Call $$b4^TCF031: return 4
SELECT secondvalue(1, true); -- Call $$b4^TCF031: return 4
SELECT secondvalue('1', 1); -- Call $$b5^TCF031: return 5
SELECT secondvalue('1', '1'); -- Call $$b6^TCF031: return 6

CREATE FUNCTION thirdvalue(integer, boolean, integer) RETURNS integer AS $$c1^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, NULL, NULL); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, NULL); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, true, NULL); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, 1, NULL); -- Error: Unknown function
CREATE FUNCTION thirdvalue(integer, boolean, numeric) RETURNS integer AS $$c2^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, NULL); -- Error: Function not unique
SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(1, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(1, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(1, true, 1.0); -- Call $$c2^TCF031: return 2
CREATE FUNCTION thirdvalue(integer, numeric, varchar) RETURNS integer AS $$c3^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, NULL); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, NULL, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, 1.0, NULL); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, NULL, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, 1.0, '1'); -- Call $$c3^TCF031: return 3
CREATE FUNCTION thirdvalue(integer, boolean, varchar) RETURNS integer AS $$c4^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(1, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, NULL); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(1, true, '1'); -- Call $$c4^TCF031: return 4
CREATE FUNCTION thirdvalue(integer, boolean, boolean) RETURNS integer AS $$c5^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, NULL); -- Error: Function not unique
SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, NULL); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(1, true, true); -- Call $$c5^TCF031: return 5
CREATE FUNCTION thirdvalue(integer, numeric, integer) RETURNS integer AS $$c6^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, 1.0, NULL); -- Error: Function not unique
SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, 1); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, NULL, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(1, 1.0, 1); -- Call $$c6^TCF031: return 6
CREATE FUNCTION thirdvalue(integer, numeric, numeric) RETURNS integer AS $$c7^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, 1.0, NULL); -- Error: Function not unique
SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, 1.0); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(1, 1.0, 1.0); -- Call $$c7^TCF031: return 7
CREATE FUNCTION thirdvalue(integer, varchar, numeric) RETURNS integer AS $$c8^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, 1.0); -- Error: Function not unique
SELECT thirdvalue(1, NULL, 1.0); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', NULL); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(1, '1', NULL); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, '1', 1.0); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(1, '1', 1.0); -- Call $$c8^TCF031: return 8
CREATE FUNCTION thirdvalue(integer, numeric, boolean) RETURNS integer AS $$c9^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, true); -- Error: Function not unique
SELECT thirdvalue(1, NULL, true); -- Error: Function not unique
SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', NULL); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, 1.0, true); -- Call $$c9^TCF031: return 9
SELECT thirdvalue(1, 1.0, true); -- Call $$c9^TCF031: return 9
CREATE FUNCTION thirdvalue(integer, varchar, varchar) RETURNS integer AS $$c10^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(1, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(1, '1', NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, '1', NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', 1.0); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, 1.0, true); -- Call $$c9^TCF031: return 9
SELECT thirdvalue(NULL, '1', '1'); -- Call $$c10^TCF031: return 10
SELECT thirdvalue(1, '1', '1'); -- Call $$c10^TCF031: return 10
CREATE FUNCTION thirdvalue(numeric, varchar, varchar) RETURNS integer AS $$c11^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, '1', '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(NULL, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', 1.0); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, 1.0, true); -- Call $$c9^TCF031: return 9
SELECT thirdvalue(1, '1', '1'); -- Call $$c10^TCF031: return 10
SELECT thirdvalue(1.0, NULL, NULL); -- Call $$c11^TCF031: return 11
SELECT thirdvalue(1.0, '1', NULL); -- Call $$c11^TCF031: return 11
SELECT thirdvalue(1.0, NULL, '1'); -- Call $$c11^TCF031: return 11
SELECT thirdvalue(1.0, '1', '1'); -- Call $$c11^TCF031: return 11
CREATE FUNCTION thirdvalue(varchar, boolean, varchar) RETURNS integer AS $$c12^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, true, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, true, '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', 1.0); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, 1.0, true); -- Call $$c9^TCF031: return 9
SELECT thirdvalue(1, '1', '1'); -- Call $$c10^TCF031: return 10
SELECT thirdvalue(1.0, NULL, NULL); -- Call $$c11^TCF031: return 11
SELECT thirdvalue('1', NULL, NULL); -- Call $$c12^TCF031: return 12
SELECT thirdvalue('1', true, NULL); -- Call $$c12^TCF031: return 12
SELECT thirdvalue('1', NULL, '1'); -- Call $$c12^TCF031: return 12
SELECT thirdvalue('1', NULL, '1'); -- Call $$c12^TCF031: return 12
SELECT thirdvalue('1', true, '1'); -- Call $$c12^TCF031: return 12
CREATE FUNCTION thirdvalue(varchar, varchar, varchar) RETURNS integer AS $$c13^TCF031;
SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not unique
SELECT thirdvalue('1', NULL, NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, '1', NULL); -- Error: Function not unique
SELECT thirdvalue(NULL, '1', '1'); -- Error: Function not unique
SELECT thirdvalue('1', NULL, '1'); -- Error: Function not unique
SELECT thirdvalue(NULL, true, 1); -- Call $$c1^TCF031: return 1
SELECT thirdvalue(NULL, true, 1.0); -- Call $$c2^TCF031: return 2
SELECT thirdvalue(NULL, 1.0, '1'); -- Call $$c3^TCF031: return 3
SELECT thirdvalue(1, true, '1'); -- Call $$c4^TCF031: return 4
SELECT thirdvalue(NULL, true, true); -- Call $$c5^TCF031: return 5
SELECT thirdvalue(NULL, 1.0, 1); -- Call $$c6^TCF031: return 6
SELECT thirdvalue(NULL, 1.0, 1.0); -- Call $$c7^TCF031: return 7
SELECT thirdvalue(NULL, '1', 1.0); -- Call $$c8^TCF031: return 8
SELECT thirdvalue(NULL, 1.0, true); -- Call $$c9^TCF031: return 9
SELECT thirdvalue(1, '1', '1'); -- Call $$c10^TCF031: return 10
SELECT thirdvalue(1.0, NULL, NULL); -- Call $$c11^TCF031: return 11
SELECT thirdvalue('1', true, NULL); -- Call $$c12^TCF031: return 12
SELECT thirdvalue('1', '1', '1'); -- Call $$c13^TCF031: return 13
CREATE FUNCTION
SAMEVALUE
1
(1 row)
SAMEVALUE
1
(1 row)
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (NUMERIC)
LINE 2: SELECT samevalue(1.0); -- Error: Unknown function
               ^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (VARCHAR)
LINE 2: SELECT samevalue('1'); -- Error: Unknown function
               ^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (BOOLEAN)
LINE 2: SELECT samevalue(true); -- Error: Unknown function
               ^^^^^^^^^
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SAMEVALUE(NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT samevalue(NULL); -- Error: Function not unique
               ^^^^^^^^^
SAMEVALUE
1
(1 row)
SAMEVALUE
2
(1 row)
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (VARCHAR)
LINE 2: SELECT samevalue('1'); -- Error: Unknown function
               ^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (BOOLEAN)
LINE 2: SELECT samevalue(true); -- Error: Unknown function
               ^^^^^^^^^
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SAMEVALUE(NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT samevalue(NULL); -- Error: Function not unique
               ^^^^^^^^^
SAMEVALUE
1
(1 row)
SAMEVALUE
2
(1 row)
SAMEVALUE
3
(1 row)
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SAMEVALUE' defined with given parameter types (BOOLEAN)
LINE 2: SELECT samevalue(true); -- Error: Unknown function
               ^^^^^^^^^
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SAMEVALUE(NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT samevalue(NULL); -- Error: Function not unique
               ^^^^^^^^^
SAMEVALUE
1
(1 row)
SAMEVALUE
2
(1 row)
SAMEVALUE
3
(1 row)
SAMEVALUE
4
(1 row)
CREATE FUNCTION
SECONDVALUE
1
(1 row)
SECONDVALUE
1
(1 row)
SECONDVALUE
1
(1 row)
SECONDVALUE
1
(1 row)
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SECONDVALUE' defined with given parameter types (NUMERIC, INTEGER)
LINE 2: SELECT secondvalue(1.0, 1); -- Error: Unknown function
               ^^^^^^^^^^^
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT secondvalue(NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(INTEGER, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue(1, NULL); --  Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SECONDVALUE' defined with given parameter types (NUMERIC, INTEGER)
LINE 2: SELECT secondvalue(1.0, 1); -- Error: Unknown function
               ^^^^^^^^^^^
SECONDVALUE
1
(1 row)
SECONDVALUE
2
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT secondvalue(NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(INTEGER, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue(1, NULL); --  Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SECONDVALUE' defined with given parameter types (VARCHAR, INTEGER)
LINE 2: SELECT secondvalue('1', 1); -- Error: Unknown function
               ^^^^^^^^^^^
SECONDVALUE
1
(1 row)
SECONDVALUE
2
(1 row)
SECONDVALUE
3
(1 row)
SECONDVALUE
3
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT secondvalue(NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(INTEGER, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue(1, NULL); --  Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'SECONDVALUE' defined with given parameter types (BOOLEAN, INTEGER)
LINE 2: SELECT secondvalue(true, 1); -- Error: Unknown function
               ^^^^^^^^^^^
SECONDVALUE
1
(1 row)
SECONDVALUE
2
(1 row)
SECONDVALUE
3
(1 row)
SECONDVALUE
4
(1 row)
SECONDVALUE
4
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT secondvalue(NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, INTEGER)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue(NULL, 1); --  Error: Function not unique
               ^^^^^^^^^^^
SECONDVALUE
1
(1 row)
SECONDVALUE
2
(1 row)
SECONDVALUE
3
(1 row)
SECONDVALUE
4
(1 row)
SECONDVALUE
4
(1 row)
SECONDVALUE
5
(1 row)
SECONDVALUE
5
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT secondvalue(NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue(NULL, '1'); --  Error: Function not unique
               ^^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'SECONDVALUE(VARCHAR, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT secondvalue('1', NULL); --  Error: Function not unique
               ^^^^^^^^^^^
SECONDVALUE
1
(1 row)
SECONDVALUE
2
(1 row)
SECONDVALUE
3
(1 row)
SECONDVALUE
4
(1 row)
SECONDVALUE
4
(1 row)
SECONDVALUE
5
(1 row)
SECONDVALUE
6
(1 row)
CREATE FUNCTION
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
[ERROR]: ERR_UNKNOWN_FUNCTION: No function 'THIRDVALUE' defined with given parameter types (NULL, INTEGER, NULL)
LINE 2: SELECT thirdvalue(NULL, 1, NULL); -- Error: Unknown function
               ^^^^^^^^^^
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, true, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
2
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, true, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, '1'); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
4
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, true, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, true, NULL); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
5
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NUMERIC, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, 1.0, NULL); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NUMERIC, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, INTEGER)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, 1); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
6
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NUMERIC, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, 1.0, NULL); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NUMERIC, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NUMERIC)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, 1.0); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
7
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NUMERIC)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, 1.0); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NUMERIC)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, 1.0); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
8
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, BOOLEAN)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, true); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, BOOLEAN)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, true); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NUMERIC, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, 1.0, NULL); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
9
(1 row)
THIRDVALUE
9
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, NULL, '1'); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(INTEGER, VARCHAR, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(1, '1', NULL); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, VARCHAR, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, '1', NULL); -- Error: Function not uniqu...
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
9
(1 row)
THIRDVALUE
10
(1 row)
THIRDVALUE
10
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, VARCHAR, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, '1', '1'); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
9
(1 row)
THIRDVALUE
10
(1 row)
THIRDVALUE
11
(1 row)
THIRDVALUE
11
(1 row)
THIRDVALUE
11
(1 row)
THIRDVALUE
11
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, BOOLEAN, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, true, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, NULL, '1'); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, BOOLEAN, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, true, '1'); -- Error: Function not uniqu...
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
9
(1 row)
THIRDVALUE
10
(1 row)
THIRDVALUE
11
(1 row)
THIRDVALUE
12
(1 row)
THIRDVALUE
12
(1 row)
THIRDVALUE
12
(1 row)
THIRDVALUE
12
(1 row)
THIRDVALUE
12
(1 row)
CREATE FUNCTION
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 1: SELECT thirdvalue(NULL, NULL, NULL); -- Error: Function not uniq...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(VARCHAR, NULL, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue('1', NULL, NULL); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, VARCHAR, NULL)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, '1', NULL); -- Error: Function not uniqu...
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(NULL, VARCHAR, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue(NULL, '1', '1'); -- Error: Function not unique
               ^^^^^^^^^^
[ERROR]: ERR_FUNCTION_NOT_UNIQUE: Function 'THIRDVALUE(VARCHAR, NULL, VARCHAR)' not unique. Parameter(s) may require explicit type cast(s)
LINE 2: SELECT thirdvalue('1', NULL, '1'); -- Error: Function not unique
               ^^^^^^^^^^
THIRDVALUE
1
(1 row)
THIRDVALUE
2
(1 row)
THIRDVALUE
3
(1 row)
THIRDVALUE
4
(1 row)
THIRDVALUE
5
(1 row)
THIRDVALUE
6
(1 row)
THIRDVALUE
7
(1 row)
THIRDVALUE
8
(1 row)
THIRDVALUE
9
(1 row)
THIRDVALUE
10
(1 row)
THIRDVALUE
11
(1 row)
THIRDVALUE
12
(1 row)
THIRDVALUE
13
(1 row)
